//*************************************************************************
// Copyright (C) 2018 Kaan Kara - Systems Group, ETH Zurich

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//*************************************************************************

#define TESTBENCH
// #define REAL_IMAGE
// #define DEBUG

#define AP_INT_MAX_W 16384

#include "hls-nn-lib.h"

#define L10_Din 512
#define L10_Cin 3
#define L10_Cout 16
#define L10_K 9
#define L10_S 1
#define L10_Ibit 16
#define L10_Wbit 16
#define L10_Mbit 16
#define L10_Abit 16
#define L10_InP 3
#define L10_OutP 16

#define L11_Din 512
#define L11_Cin 16
#define L11_Cout 32
#define L11_K 7
#define L11_S 1
#define L11_PK 2
#define L11_PS 2
#define L11_Ibit 16
#define L11_Wbit 16
#define L11_Mbit 16
#define L11_Abit 16
#define L11_InP 16
#define L11_OutP 16

#define L12_Din 256
#define L12_Cin 32
#define L12_Cout 16
#define L12_K 7
#define L12_S 1
#define L12_PK 2
#define L12_PS 2
#define L12_Ibit 16
#define L12_Wbit 16
#define L12_Mbit 16
#define L12_Abit 16
#define L12_InP 32
#define L12_OutP 16

#define L13_Din 128
#define L13_Cin 16
#define L13_Cout 8
#define L13_K 7
#define L13_S 1
#define L13_Ibit 16
#define L13_Wbit 16
#define L13_Mbit 16
#define L13_Abit 16
#define L13_InP 16
#define L13_OutP 8

#define L20_Din 512
#define L20_Cin 3
#define L20_Cout 20
#define L20_K 7
#define L20_S 1
#define L20_Ibit 16
#define L20_Wbit 16
#define L20_Mbit 16
#define L20_Abit 16
#define L20_InP 3
#define L20_OutP 10

#define L21_Din 512
#define L21_Cin 20
#define L21_Cout 40
#define L21_K 5
#define L21_S 1
#define L21_PK 2
#define L21_PS 2
#define L21_Ibit 16
#define L21_Wbit 16
#define L21_Mbit 16
#define L21_Abit 16
#define L21_InP 20
#define L21_OutP 10

#define L22_Din 256
#define L22_Cin 40
#define L22_Cout 20
#define L22_K 7
#define L22_S 1
#define L22_PK 2
#define L22_PS 2
#define L22_Ibit 16
#define L22_Wbit 16
#define L22_Mbit 16
#define L22_Abit 16
#define L22_InP 20
#define L22_OutP 10

#define L23_Din 128
#define L23_Cin 20
#define L23_Cout 10
#define L23_K 7
#define L23_S 1
#define L23_PK 2
#define L23_PS 2
#define L23_Ibit 16
#define L23_Wbit 16
#define L23_Mbit 16
#define L23_Abit 16
#define L23_InP 20
#define L23_OutP 10

#define L30_Din 512
#define L30_Cin 3
#define L30_Cout 24
#define L30_K 5
#define L30_S 1
#define L30_Ibit 16
#define L30_Wbit 16
#define L30_Mbit 16
#define L30_Abit 16
#define L30_InP 3
#define L30_OutP 12

#define L31_Din 512
#define L31_Cin 24
#define L31_Cout 48
#define L31_K 3
#define L31_S 1
#define L31_PK 2
#define L31_PS 2
#define L31_Ibit 16
#define L31_Wbit 16
#define L31_Mbit 16
#define L31_Abit 16
#define L31_InP 24
#define L31_OutP 12

#define L32_Din 256
#define L32_Cin 48
#define L32_Cout 24
#define L32_K 3
#define L32_S 1
#define L32_PK 2
#define L32_PS 2
#define L32_Ibit 16
#define L32_Wbit 16
#define L32_Mbit 16
#define L32_Abit 16
#define L32_InP 24
#define L32_OutP 12

#define L33_Din 128
#define L33_Cin 24
#define L33_Cout 12
#define L33_K 3
#define L33_S 1
#define L33_PK 2
#define L33_PS 2
#define L33_Ibit 16
#define L33_Wbit 16
#define L33_Mbit 16
#define L33_Abit 16
#define L33_InP 24
#define L33_OutP 12 

#define L_Wbit 16
#define L_Ibit 16
#define L_Mbit 16
#define L_Abit 16
#define SCALE_BITS 1
#define FACTOR_SCALE_BITS 10

#define L10_WEIGHT_ITERATIONS ((L10_Cin*L10_K*L10_K)/L10_InP)*(L10_Cout/L10_OutP)
#define L10_FACTOR_ITERATIONS (L10_Cout/L10_OutP)

#define L11_WEIGHT_ITERATIONS ((L11_Cin*L11_K*L11_K)/L11_InP)*(L11_Cout/L11_OutP)
#define L11_FACTOR_ITERATIONS (L11_Cout/L11_OutP)

#define L12_WEIGHT_ITERATIONS ((L12_Cin*L12_K*L12_K)/L12_InP)*(L12_Cout/L12_OutP)
#define L12_FACTOR_ITERATIONS (L12_Cout/L12_OutP)

#define L13_WEIGHT_ITERATIONS ((L13_Cin*L13_K*L13_K)/L13_InP)*(L13_Cout/L13_OutP)
#define L13_FACTOR_ITERATIONS (L13_Cout/L13_OutP)

#define L20_WEIGHT_ITERATIONS ((L20_Cin*L20_K*L20_K)/L20_InP)*(L20_Cout/L20_OutP)
#define L20_FACTOR_ITERATIONS (L20_Cout/L20_OutP)

#define L21_WEIGHT_ITERATIONS ((L21_Cin*L21_K*L21_K)/L21_InP)*(L21_Cout/L21_OutP)
#define L21_FACTOR_ITERATIONS (L21_Cout/L21_OutP)

#define L22_WEIGHT_ITERATIONS ((L22_Cin*L22_K*L22_K)/L22_InP)*(L22_Cout/L22_OutP)
#define L22_FACTOR_ITERATIONS (L22_Cout/L22_OutP)

#define L23_WEIGHT_ITERATIONS ((L23_Cin*L23_K*L23_K)/L23_InP)*(L23_Cout/L23_OutP)
#define L23_FACTOR_ITERATIONS (L23_Cout/L23_OutP)

#define L30_WEIGHT_ITERATIONS ((L30_Cin*L30_K*L30_K)/L30_InP)*(L30_Cout/L30_OutP)
#define L30_FACTOR_ITERATIONS (L30_Cout/L30_OutP)

#define L31_WEIGHT_ITERATIONS ((L31_Cin*L31_K*L31_K)/L31_InP)*(L31_Cout/L31_OutP)
#define L31_FACTOR_ITERATIONS (L31_Cout/L31_OutP)

#define L32_WEIGHT_ITERATIONS ((L32_Cin*L32_K*L32_K)/L32_InP)*(L32_Cout/L32_OutP)
#define L32_FACTOR_ITERATIONS (L32_Cout/L32_OutP)

#define L33_WEIGHT_ITERATIONS ((L33_Cin*L33_K*L33_K)/L33_InP)*(L33_Cout/L33_OutP)
#define L33_FACTOR_ITERATIONS (L33_Cout/L33_OutP)


static ap_uint<L10_InP*L10_Wbit> weights10[L10_OutP][L10_WEIGHT_ITERATIONS];
static ap_int<L_Mbit> factorA10[L10_OutP][L10_FACTOR_ITERATIONS];
static ap_int<L_Mbit> factorB10[L10_OutP][L10_FACTOR_ITERATIONS];

static ap_uint<L11_InP*L11_Wbit> weights11[L11_OutP][L11_WEIGHT_ITERATIONS];
static ap_int<L_Mbit> factorA11[L11_OutP][L11_FACTOR_ITERATIONS];
static ap_int<L_Mbit> factorB11[L11_OutP][L11_FACTOR_ITERATIONS];

static ap_uint<L12_InP*L12_Wbit> weights12[L12_OutP][L12_WEIGHT_ITERATIONS];
static ap_int<L_Mbit> factorA12[L12_OutP][L12_FACTOR_ITERATIONS];
static ap_int<L_Mbit> factorB12[L12_OutP][L12_FACTOR_ITERATIONS];

static ap_uint<L13_InP*L13_Wbit> weights13[L13_OutP][L13_WEIGHT_ITERATIONS];
static ap_int<L_Mbit> factorA13[L13_OutP][L13_FACTOR_ITERATIONS];
static ap_int<L_Mbit> factorB13[L13_OutP][L13_FACTOR_ITERATIONS];

static ap_uint<L20_InP*L20_Wbit> weights20[L20_OutP][L20_WEIGHT_ITERATIONS];
static ap_int<L_Mbit> factorA20[L20_OutP][L20_FACTOR_ITERATIONS];
static ap_int<L_Mbit> factorB20[L20_OutP][L20_FACTOR_ITERATIONS];

static ap_uint<L21_InP*L21_Wbit> weights21[L21_OutP][L21_WEIGHT_ITERATIONS];
static ap_int<L_Mbit> factorA21[L21_OutP][L21_FACTOR_ITERATIONS];
static ap_int<L_Mbit> factorB21[L21_OutP][L21_FACTOR_ITERATIONS];

static ap_uint<L22_InP*L22_Wbit> weights22[L22_OutP][L22_WEIGHT_ITERATIONS];
static ap_int<L_Mbit> factorA22[L22_OutP][L22_FACTOR_ITERATIONS];
static ap_int<L_Mbit> factorB22[L22_OutP][L22_FACTOR_ITERATIONS];

static ap_uint<L23_InP*L23_Wbit> weights23[L23_OutP][L23_WEIGHT_ITERATIONS];
static ap_int<L_Mbit> factorA23[L23_OutP][L23_FACTOR_ITERATIONS];
static ap_int<L_Mbit> factorB23[L23_OutP][L23_FACTOR_ITERATIONS];

static ap_uint<L30_InP*L30_Wbit> weights30[L30_OutP][L30_WEIGHT_ITERATIONS];
static ap_int<L_Mbit> factorA30[L30_OutP][L30_FACTOR_ITERATIONS];
static ap_int<L_Mbit> factorB30[L30_OutP][L30_FACTOR_ITERATIONS];

static ap_uint<L31_InP*L31_Wbit> weights31[L31_OutP][L31_WEIGHT_ITERATIONS];
static ap_int<L_Mbit> factorA31[L31_OutP][L31_FACTOR_ITERATIONS];
static ap_int<L_Mbit> factorB31[L31_OutP][L31_FACTOR_ITERATIONS];

static ap_uint<L32_InP*L32_Wbit> weights32[L32_OutP][L32_WEIGHT_ITERATIONS];
static ap_int<L_Mbit> factorA32[L32_OutP][L32_FACTOR_ITERATIONS];
static ap_int<L_Mbit> factorB32[L32_OutP][L32_FACTOR_ITERATIONS];

static ap_uint<L33_InP*L33_Wbit> weights33[L33_OutP][L33_WEIGHT_ITERATIONS];
static ap_int<L_Mbit> factorA33[L33_OutP][L33_FACTOR_ITERATIONS];
static ap_int<L_Mbit> factorB33[L33_OutP][L33_FACTOR_ITERATIONS];


    template <unsigned LineWidth, unsigned NumLines>
void CopyStream3 (
        stream<ap_uint<LineWidth> >& in, 
        stream<ap_uint<LineWidth> >& out1, 
        stream<ap_uint<LineWidth> >& out2, 
        stream<ap_uint<LineWidth> >& out3)
{
    for (unsigned i = 0; i < NumLines; i++) {
        ap_uint<LineWidth> temp = in.read();
        out1.write(temp);
        out2.write(temp);
        out3.write(temp);
    }
}

template <unsigned LineWidth1,
         unsigned LineWidth2,
         unsigned LineWidth3,
    unsigned NumLines>
void ConcatStream3 (
        stream<ap_uint<LineWidth1> >& in1, 
        stream<ap_uint<LineWidth2> >& in2, 
        stream<ap_uint<LineWidth3> >& in3,
        stream<ap_uint<LineWidth1+LineWidth2+LineWidth3> >& out)
{
    for (unsigned i = 0; i < NumLines; i++) {
        ap_uint<LineWidth1+LineWidth2+LineWidth3> temp;
        ap_uint<LineWidth1> temp1 = in1.read();
        ap_uint<LineWidth2> temp2 = in2.read();
        ap_uint<LineWidth3> temp3 = in3.read();
        temp(LineWidth1-1,0) = temp1;
        temp(LineWidth1+LineWidth2-1,LineWidth1) = temp2;
        temp(LineWidth1+LineWidth2+LineWidth3-1,LineWidth1+LineWidth2) = temp3;
        out.write(temp);
    }
}

void DoConv(stream<ap_axis >& in, stream<ap_axis >& out)
{
#pragma HLS DATAFLOW
    stream<ap_uint<384> > in_stream_extract("extract");
    const unsigned in_reps = L10_Din*L10_Din*L10_Cin*L10_Ibit/384;
    ExtractPixels<384, in_reps> (in, in_stream_extract);

    stream<ap_uint<L10_Cin*L10_Ibit> > in_stream("in_stream");
    ReduceWidth<384, L10_Cin*L10_Ibit, in_reps> (in_stream_extract, in_stream);

    stream<ap_uint<L10_Cin*L10_Ibit> > in1_stream("in1");
    stream<ap_uint<L10_Cin*L10_Ibit> > in2_stream("in2");
    stream<ap_uint<L10_Cin*L10_Ibit> > in3_stream("in3");
    CopyStream3<L10_Cin*L10_Ibit, L10_Din*L10_Din>(in_stream, in1_stream, in2_stream, in3_stream);

    // BRANCH 1
    //conv10
    stream<ap_uint<L10_Cout*L11_Ibit> > conv10("conv10");
    CONV2D_ACT_NoP<L10_K, L10_S, L10_Din, L10_Cin, L10_Cout, 
        L10_Ibit, L10_Wbit, L10_Mbit, L11_Ibit, 
        L10_InP, L10_OutP, SCALE_BITS, FACTOR_SCALE_BITS>
            (in1_stream, weights10, factorA10, factorB10, conv10, 1);

    //conv11
    stream<ap_uint<L11_Cout*L12_Ibit> > conv11("conv11");
    CONV2D_ACT_NoP<L11_K, L11_S, L11_Din, L11_Cin, L11_Cout, 
        L11_Ibit, L11_Wbit, L11_Mbit, L12_Ibit, 
        L11_InP, L11_OutP, SCALE_BITS, FACTOR_SCALE_BITS>
            (conv10, weights11, factorA11, factorB11, conv11, 1);
    stream<ap_uint<L11_Cout*L12_Ibit> > pool11("pool11");
    POOL2D_NoP<L11_PK, L11_PS, L11_Din, L11_Cout, L12_Ibit> (conv11, pool11, 1);

    //conv12
    stream<ap_uint<L12_Cout*L13_Ibit> > conv12("conv12");
    CONV2D_ACT_NoP<L12_K, L12_S, L12_Din, L12_Cin, L12_Cout, 
        L12_Ibit, L12_Wbit, L12_Mbit, L13_Ibit, 
        L12_InP, L12_OutP, SCALE_BITS, FACTOR_SCALE_BITS>
            (pool11, weights12, factorA12, factorB12, conv12, 1);
    stream<ap_uint<L12_Cout*L13_Ibit> > pool12("pool12");
    POOL2D_NoP<L12_PK, L12_PS, L12_Din, L12_Cout, L13_Ibit> (conv12, pool12, 1);

    //conv13
    stream<ap_uint<L13_Cout*L_Abit> > conv13("conv13");
    CONV2D_ACT_NoP<L13_K, L13_S, L13_Din, L13_Cin, L13_Cout, 
        L13_Ibit, L13_Wbit, L13_Mbit, L_Abit, 
        L13_InP, L13_OutP, SCALE_BITS, FACTOR_SCALE_BITS>
            (pool12, weights13, factorA13, factorB13, conv13, 1);

    // BRANCH 2
    //conv20
    stream<ap_uint<L20_Cout*L21_Ibit> > conv20("conv20");
    CONV2D_ACT_NoP<L20_K, L20_S, L20_Din, L20_Cin, L20_Cout, 
        L20_Ibit, L20_Wbit, L20_Mbit, L21_Ibit, 
        L20_InP, L20_OutP, SCALE_BITS, FACTOR_SCALE_BITS>
            (in1_stream, weights20, factorA20, factorB20, conv20, 1);

    //conv21
    stream<ap_uint<L21_Cout*L22_Ibit> > conv21("conv21");
    CONV2D_ACT_NoP<L21_K, L21_S, L21_Din, L21_Cin, L21_Cout, 
        L21_Ibit, L21_Wbit, L21_Mbit, L22_Ibit, 
        L21_InP, L21_OutP, SCALE_BITS, FACTOR_SCALE_BITS>
            (conv20, weights21, factorA21, factorB21, conv21, 1);
    stream<ap_uint<L21_Cout*L22_Ibit> > pool21("pool21");
    POOL2D_NoP<L21_PK, L21_PS, L21_Din, L21_Cout, L22_Ibit> (conv21, pool21, 1);

    //conv22
    stream<ap_uint<L22_Cout*L23_Ibit> > conv22("conv22");
    CONV2D_ACT_NoP<L22_K, L22_S, L22_Din, L22_Cin, L22_Cout, 
        L22_Ibit, L22_Wbit, L22_Mbit, L23_Ibit, 
        L22_InP, L22_OutP, SCALE_BITS, FACTOR_SCALE_BITS>
            (pool21, weights22, factorA22, factorB22, conv22, 1);
    stream<ap_uint<L22_Cout*L23_Ibit> > pool22("pool22");
    POOL2D_NoP<L22_PK, L22_PS, L22_Din, L22_Cout, L23_Ibit> (conv22, pool22, 1);

    //conv23
    stream<ap_uint<L23_Cout*L_Abit> > conv23("conv23");
    CONV2D_ACT_NoP<L23_K, L23_S, L23_Din, L23_Cin, L23_Cout, 
        L23_Ibit, L23_Wbit, L23_Mbit, L_Abit, 
        L23_InP, L23_OutP, SCALE_BITS, FACTOR_SCALE_BITS>
            (pool22, weights23, factorA23, factorB23, conv23, 1);

    // BRANCH 3
    //conv30
    stream<ap_uint<L30_Cout*L31_Ibit> > conv30("conv30");
    CONV2D_ACT_NoP<L30_K, L30_S, L30_Din, L30_Cin, L30_Cout, 
        L30_Ibit, L30_Wbit, L30_Mbit, L31_Ibit, 
        L30_InP, L30_OutP, SCALE_BITS, FACTOR_SCALE_BITS>
            (in1_stream, weights30, factorA30, factorB30, conv30, 1);

    //conv31
    stream<ap_uint<L31_Cout*L32_Ibit> > conv31("conv31");
    CONV2D_ACT_NoP<L31_K, L31_S, L31_Din, L31_Cin, L31_Cout, 
        L31_Ibit, L31_Wbit, L31_Mbit, L32_Ibit, 
        L31_InP, L31_OutP, SCALE_BITS, FACTOR_SCALE_BITS>
            (conv30, weights31, factorA31, factorB31, conv31, 1);
    stream<ap_uint<L31_Cout*L32_Ibit> > pool31("pool31");
    POOL2D_NoP<L31_PK, L31_PS, L31_Din, L31_Cout, L32_Ibit> (conv31, pool31, 1);

    //conv32
    stream<ap_uint<L32_Cout*L33_Ibit> > conv32("conv32");
    CONV2D_ACT_NoP<L32_K, L32_S, L32_Din, L32_Cin, L32_Cout, 
        L32_Ibit, L32_Wbit, L32_Mbit, L33_Ibit, 
        L32_InP, L32_OutP, SCALE_BITS, FACTOR_SCALE_BITS>
            (pool31, weights32, factorA32, factorB32, conv32, 1);
    stream<ap_uint<L32_Cout*L33_Ibit> > pool32("pool32");
    POOL2D_NoP<L32_PK, L32_PS, L32_Din, L32_Cout, L33_Ibit> (conv32, pool32, 1);

    //conv33
    stream<ap_uint<L33_Cout*L_Abit> > conv33("conv33");
    CONV2D_ACT_NoP<L33_K, L33_S, L33_Din, L33_Cin, L33_Cout, 
        L33_Ibit, L33_Wbit, L33_Mbit, L_Abit, 
        L33_InP, L33_OutP, SCALE_BITS, FACTOR_SCALE_BITS>
            (pool32, weights33, factorA33, factorB33, conv33, 1);

    //Concat
    stream<ap_uint<(L13_Cout+L23_Cout+L33_Cout)*L_Abit> > concat("concat");
    ConcatStream3<L13_Cout*L_Abit,L23_Cout*L_Abit,
        L33_Cout*L_Abit,L33_Din*L33_Din>
            (conv13,conv23,conv33,concat);
    stream<ap_uint<512> > out_padded("out_padded");
    AppendZeros<(L13_Cout+L23_Cout+L33_Cout)*L_Abit,512,L33_Din*L33_Din>
        (concat, out_padded, 1);
    AddLast<L33_Din*L33_Din>(out_padded, out, 1);
}

void writeWeightsFactors(stream<ap_axis >& in) {
#pragma HLS DATAFLOW
    for (unsigned i = 0; i < L10_WEIGHT_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        for (unsigned p = 0; p < L10_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<512> temp_in = in.read().data;
            ap_uint<L10_InP*L10_Wbit> temp = temp_in(L10_InP*L10_Wbit-1, 0);
            weights10[p][i] = temp;
        }
    }

    for (unsigned i = 0; i < L10_FACTOR_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        ap_uint<512> temp_in = in.read().data;
        for (unsigned p = 0; p < L10_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<2*L10_Mbit> temp_factorAB = temp_in( (p+1)*2*L10_Mbit-1, p*2*L10_Mbit );
            factorA10[p][i] = temp_factorAB(L10_Mbit-1, 0);
            factorB10[p][i] = temp_factorAB(2*L10_Mbit-1, L10_Mbit);
        }
    }

    for (unsigned i = 0; i < L11_WEIGHT_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        for (unsigned p = 0; p < L11_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<512> temp_in = in.read().data;
            ap_uint<L11_InP*L11_Wbit> temp = temp_in(L11_InP*L11_Wbit-1,0);
            weights11[p][i] = temp;
        }
    }

    for (unsigned i = 0; i < L11_FACTOR_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        ap_uint<512> temp_in = in.read().data;
        for (unsigned p = 0; p < L11_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<2*L11_Mbit> temp_factorAB = temp_in( (p+1)*2*L11_Mbit-1, p*2*L11_Mbit );
            factorA11[p][i] = temp_factorAB(L11_Mbit-1, 0);
            factorB11[p][i] = temp_factorAB(2*L11_Mbit-1, L11_Mbit);
        }
    }

    for (unsigned i = 0; i < L12_WEIGHT_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        for (unsigned p = 0; p < L12_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<512> temp_in = in.read().data;
            ap_uint<L12_InP*L12_Wbit> temp = temp_in(L12_InP*L12_Wbit-1,0);
            weights12[p][i] = temp;
        }
    }

    for (unsigned i = 0; i < L12_FACTOR_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        ap_uint<512> temp_in = in.read().data;
        for (unsigned p = 0; p < L12_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<2*L12_Mbit> temp_factorAB = temp_in( (p+1)*2*L12_Mbit-1, p*2*L12_Mbit );
            factorA12[p][i] = temp_factorAB(L12_Mbit-1, 0);
            factorB12[p][i] = temp_factorAB(2*L12_Mbit-1, L12_Mbit);
        }
    }

    for (unsigned i = 0; i < L13_WEIGHT_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        for (unsigned p = 0; p < L13_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<512> temp_in = in.read().data;
            ap_uint<L13_InP*L13_Wbit> temp = temp_in(L13_InP*L13_Wbit-1,0);
            weights13[p][i] = temp;
        }
    }

    for (unsigned i = 0; i < L13_FACTOR_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        ap_uint<512> temp_in = in.read().data;
        for (unsigned p = 0; p < L13_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<2*L13_Mbit> temp_factorAB = temp_in( (p+1)*2*L13_Mbit-1, p*2*L13_Mbit );
            factorA13[p][i] = temp_factorAB(L13_Mbit-1, 0);
            factorB13[p][i] = temp_factorAB(2*L13_Mbit-1, L13_Mbit);
        }
    }

    for (unsigned i = 0; i < L20_WEIGHT_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        for (unsigned p = 0; p < L20_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<512> temp_in = in.read().data;
            ap_uint<L20_InP*L20_Wbit> temp = temp_in(L20_InP*L20_Wbit-1,0);
            weights20[p][i] = temp;
        }
    }

    for (unsigned i = 0; i < L20_FACTOR_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        ap_uint<512> temp_in = in.read().data;
        for (unsigned p = 0; p < L20_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<2*L20_Mbit> temp_factorAB = temp_in( (p+1)*2*L20_Mbit-1, p*2*L20_Mbit );
            factorA20[p][i] = temp_factorAB(L20_Mbit-1, 0);
            factorB20[p][i] = temp_factorAB(2*L20_Mbit-1, L20_Mbit);
        }
    }

    for (unsigned i = 0; i < L21_WEIGHT_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        for (unsigned p = 0; p < L21_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<512> temp_in = in.read().data;
            ap_uint<L21_InP*L21_Wbit> temp = temp_in(L21_InP*L21_Wbit-1,0);
            weights21[p][i] = temp;
        }
    }

    for (unsigned i = 0; i < L21_FACTOR_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        ap_uint<512> temp_in = in.read().data;
        for (unsigned p = 0; p < L21_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<2*L21_Mbit> temp_factorAB = temp_in( (p+1)*2*L21_Mbit-1, p*2*L21_Mbit );
            factorA21[p][i] = temp_factorAB(L21_Mbit-1, 0);
            factorB21[p][i] = temp_factorAB(2*L21_Mbit-1, L21_Mbit);
        }
    }

    for (unsigned i = 0; i < L22_WEIGHT_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        for (unsigned p = 0; p < L22_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<512> temp_in = in.read().data;
            ap_uint<L22_InP*L22_Wbit> temp = temp_in(L22_InP*L22_Wbit-1,0);
            weights22[p][i] = temp;
        }
    }

    for (unsigned i = 0; i < L22_FACTOR_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        ap_uint<512> temp_in = in.read().data;
        for (unsigned p = 0; p < L22_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<2*L22_Mbit> temp_factorAB = temp_in( (p+1)*2*L22_Mbit-1, p*2*L22_Mbit );
            factorA22[p][i] = temp_factorAB(L22_Mbit-1, 0);
            factorB22[p][i] = temp_factorAB(2*L22_Mbit-1, L22_Mbit);
        }
    }

    for (unsigned i = 0; i < L23_WEIGHT_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        for (unsigned p = 0; p < L23_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<512> temp_in = in.read().data;
            ap_uint<L23_InP*L23_Wbit> temp = temp_in(L23_InP*L23_Wbit-1,0);
            weights23[p][i] = temp;
        }
    }

    for (unsigned i = 0; i < L23_FACTOR_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        ap_uint<512> temp_in = in.read().data;
        for (unsigned p = 0; p < L23_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<2*L23_Mbit> temp_factorAB = temp_in( (p+1)*2*L23_Mbit-1, p*2*L23_Mbit );
            factorA23[p][i] = temp_factorAB(L23_Mbit-1, 0);
            factorB23[p][i] = temp_factorAB(2*L23_Mbit-1, L23_Mbit);
        }
    }

    for (unsigned i = 0; i < L30_WEIGHT_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        for (unsigned p = 0; p < L30_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<512> temp_in = in.read().data;
            ap_uint<L30_InP*L30_Wbit> temp = temp_in(L30_InP*L30_Wbit-1,0);
            weights30[p][i] = temp;
        }
    }

    for (unsigned i = 0; i < L30_FACTOR_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        ap_uint<512> temp_in = in.read().data;
        for (unsigned p = 0; p < L30_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<2*L30_Mbit> temp_factorAB = temp_in( (p+1)*2*L30_Mbit-1, p*2*L30_Mbit );
            factorA30[p][i] = temp_factorAB(L30_Mbit-1, 0);
            factorB30[p][i] = temp_factorAB(2*L30_Mbit-1, L30_Mbit);
        }
    }

    for (unsigned i = 0; i < L31_WEIGHT_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        for (unsigned p = 0; p < L31_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<512> temp_in = in.read().data;
            ap_uint<L31_InP*L31_Wbit> temp = temp_in(L31_InP*L31_Wbit-1,0);
            weights31[p][i] = temp;
        }
    }

    for (unsigned i = 0; i < L31_FACTOR_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        ap_uint<512> temp_in = in.read().data;
        for (unsigned p = 0; p < L31_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<2*L31_Mbit> temp_factorAB = temp_in( (p+1)*2*L31_Mbit-1, p*2*L31_Mbit );
            factorA31[p][i] = temp_factorAB(L31_Mbit-1, 0);
            factorB31[p][i] = temp_factorAB(2*L31_Mbit-1, L31_Mbit);
        }
    }

    for (unsigned i = 0; i < L32_WEIGHT_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        for (unsigned p = 0; p < L32_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<512> temp_in = in.read().data;
            ap_uint<L32_InP*L32_Wbit> temp = temp_in(L32_InP*L32_Wbit-1,0);
            weights32[p][i] = temp;
        }
    }

    for (unsigned i = 0; i < L32_FACTOR_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        ap_uint<512> temp_in = in.read().data;
        for (unsigned p = 0; p < L32_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<2*L32_Mbit> temp_factorAB = temp_in( (p+1)*2*L32_Mbit-1, p*2*L32_Mbit );
            factorA32[p][i] = temp_factorAB(L32_Mbit-1, 0);
            factorB32[p][i] = temp_factorAB(2*L32_Mbit-1, L32_Mbit);
        }
    }

    for (unsigned i = 0; i < L33_WEIGHT_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        for (unsigned p = 0; p < L33_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<512> temp_in = in.read().data;
            ap_uint<L33_InP*L33_Wbit> temp = temp_in(L33_InP*L33_Wbit-1,0);
            weights33[p][i] = temp;
        }
    }

    for (unsigned i = 0; i < L33_FACTOR_ITERATIONS; i++) {
#pragma HLS PIPELINE II=1
        ap_uint<512> temp_in = in.read().data;
        for (unsigned p = 0; p < L33_OutP; p++) {
#pragma HLS UNROLL
            ap_uint<2*L33_Mbit> temp_factorAB = temp_in( (p+1)*2*L33_Mbit-1, p*2*L33_Mbit );
            factorA33[p][i] = temp_factorAB(L33_Mbit-1, 0);
            factorB33[p][i] = temp_factorAB(2*L33_Mbit-1, L33_Mbit);
        }
    }
}

void halfsqueezenet(stream<ap_axis >& in, stream<ap_axis >& out,
        const unsigned what) {
#pragma HLS INTERFACE axis register both port=out
#pragma HLS INTERFACE axis register both port=in
#pragma HLS INTERFACE s_axilite port=what bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

#pragma HLS RESOURCE variable=weights10 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorA10 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorB10 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights10 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorA10 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorB10 complete dim=0

#pragma HLS RESOURCE variable=weights11 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorA11 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorB11 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights11 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorA11 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorB11 complete dim=0

#pragma HLS RESOURCE variable=weights12 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorA12 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorB12 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights12 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorA12 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorB12 complete dim=0

#pragma HLS RESOURCE variable=weights13 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorA13 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorB13 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights13 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorA13 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorB13 complete dim=0

#pragma HLS RESOURCE variable=weights20 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorA20 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorB20 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights20 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorA20 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorB20 complete dim=0

#pragma HLS RESOURCE variable=weights21 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorA21 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorB21 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights21 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorA21 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorB21 complete dim=0

#pragma HLS RESOURCE variable=weights22 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorA22 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorB22 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights22 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorA22 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorB22 complete dim=0

#pragma HLS RESOURCE variable=weights23 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorA23 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorB23 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights23 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorA23 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorB23 complete dim=0

#pragma HLS RESOURCE variable=weights30 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorA30 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorB30 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights30 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorA30 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorB30 complete dim=0

#pragma HLS RESOURCE variable=weights31 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorA31 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorB31 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights31 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorA31 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorB31 complete dim=0

#pragma HLS RESOURCE variable=weights32 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorA32 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorB32 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights32 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorA32 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorB32 complete dim=0

#pragma HLS RESOURCE variable=weights33 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorA33 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=factorB33 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights33 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorA33 complete dim=0
#pragma HLS ARRAY_PARTITION variable=factorB33 complete dim=0

    if (what == 0) {
        writeWeightsFactors(in);
    }
    else {
        DoConv(in, out);
    }
}

// TESTBENCH
#ifdef TESTBENCH

#ifdef REAL_IMAGE
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace cv;
#endif

#include <fstream>
#include <iostream>
#include <string>
using namespace std;
ap_uint<16> get_weights(unsigned l, unsigned i, unsigned p){
    ap_uint<16> data = random()%512;
    return data;
}

ap_uint<16> get_factorA(unsigned l, unsigned i, unsigned p){
    ap_uint<16> data = random()%512;
    return data;
}

ap_uint<16> get_factorB(unsigned l, unsigned i, unsigned p){
    ap_uint<16> data = random()%512;
    return data;
}

int main() {

    const unsigned NUM_SAMPLES=1;

#ifdef REAL_IMAGE
    string imagename("test_image1.png");
    Mat im;
    im = imread(imagename.c_str(), IMREAD_COLOR);

    unsigned height = im.rows;
    unsigned width = im.cols;
#else
    unsigned height = 512;
    unsigned width = 512;
#endif

    cout << "Image height: " << height << endl;
    cout << "Image width: " << width << endl;

    unsigned L0_Ibit = L10_Ibit;
    unsigned L0_Cin = L10_Cin;
    const unsigned pixel_bits = L0_Ibit*L0_Cin;
    const unsigned pixels_per_line = 384/pixel_bits;
    const unsigned buffer_size = (NUM_SAMPLES*height*width)/pixels_per_line;
    stream<ap_axis > inputStream("inputStream");

    cout << "pixels_per_line: " << pixels_per_line << endl;
    cout << "buffer_size: " << buffer_size << endl;

#ifdef REAL_IMAGE
    uint8_t* pixel_ptr = (uint8_t*)im.data;
    unsigned channels = im.channels();
#else
    uint8_t* pixel_ptr = (uint8_t*)malloc(3*height*width);
    unsigned channels = 3;
    unsigned k = 0;
    for (unsigned y = 0; y < height; y++) {
        for (unsigned x = 0; x < width; x++) {
            for (unsigned c = 0; c < channels; c++) {
                pixel_ptr[y*width*channels + x*channels + c] = (k++)%256;
                // pixel_ptr[y*width*channels + x*channels + c] = 0;
            }			
        }
    }
#endif
    unsigned index = 0;
    unsigned word;

    for (unsigned i = 0; i < NUM_SAMPLES; i++) {
        word = 0;
        ap_axis temp;
        for (unsigned y = 0; y < height; y++) {
            for (unsigned x = 0; x < width; x++) {
                unsigned red = (unsigned)pixel_ptr[y*width*channels + x*channels];
                unsigned green = (unsigned)pixel_ptr[y*width*channels + x*channels + 1];
                unsigned blue = (unsigned)pixel_ptr[y*width*channels + x*channels + 2];
                unsigned rgb = (blue << 16) + (green << 8) + red;

                temp.data(pixel_bits*(word+1)-1, pixel_bits*word) = rgb;

                if (word == pixels_per_line-1) {
                    inputStream.write(temp);
                    word = 0;
                    temp.data = 0;
                    index++;
                }
                else
                    word++;
            }
        }
    }

#ifndef REAL_IMAGE
    free(pixel_ptr);
#endif

    stream<ap_axis> weightsfactors_stream;


    ofstream ofs ("weights_file.txt", ofstream::out);
    for (unsigned i = 0; i < L10_WEIGHT_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L10_OutP; p++) {
            temp.data(L10_InP*L10_Wbit-1, 0) = get_weights(10,i,p);
            weightsfactors_stream.write(temp);
        }
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L10_FACTOR_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L10_OutP; p++) {
            temp.data( 2*p*L10_Mbit + L10_Mbit-1, 2*p*L10_Mbit ) = get_factorA(10,i,p);
            temp.data( (2*p+1)*L10_Mbit + L10_Mbit-1, (2*p+1)*L10_Mbit ) = get_factorB(10,i,p);
        }
        weightsfactors_stream.write(temp);
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L11_WEIGHT_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L11_OutP; p++) {
            temp.data(L11_InP*L11_Wbit-1, 0) = get_weights(11,i,p);
            weightsfactors_stream.write(temp);
        }
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L11_FACTOR_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L11_OutP; p++) {
            temp.data( 2*p*L11_Mbit + L11_Mbit-1, 2*p*L11_Mbit ) = get_factorA(11,i,p);
            temp.data( (2*p+1)*L11_Mbit + L11_Mbit-1, (2*p+1)*L11_Mbit ) = get_factorB(11,i,p);
        }
        weightsfactors_stream.write(temp);
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L12_WEIGHT_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L12_OutP; p++) {
            temp.data(L12_InP*L12_Wbit-1, 0) = get_weights(12,i,p);
            weightsfactors_stream.write(temp);
        }
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L12_FACTOR_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L12_OutP; p++) {
            temp.data( 2*p*L12_Mbit + L12_Mbit-1, 2*p*L12_Mbit ) = get_factorA(12,i,p);
            temp.data( (2*p+1)*L12_Mbit + L12_Mbit-1, (2*p+1)*L12_Mbit ) = get_factorB(12,i,p);
        }
        weightsfactors_stream.write(temp);
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L13_WEIGHT_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L13_OutP; p++) {
            temp.data(L13_InP*L13_Wbit-1, 0) = get_weights(13,i,p);
            weightsfactors_stream.write(temp);
        }
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L13_FACTOR_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L13_OutP; p++) {
            temp.data( 2*p*L13_Mbit + L13_Mbit-1, 2*p*L13_Mbit ) = get_factorA(13,i,p);
            temp.data( (2*p+1)*L13_Mbit + L13_Mbit-1, (2*p+1)*L13_Mbit ) = get_factorB(13,i,p);
        }
        weightsfactors_stream.write(temp);
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L20_WEIGHT_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L20_OutP; p++) {
            temp.data(L20_InP*L20_Wbit-1, 0) = get_weights(20,i,p);
            weightsfactors_stream.write(temp);
        }
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L20_FACTOR_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L20_OutP; p++) {
            temp.data( 2*p*L20_Mbit + L20_Mbit-1, 2*p*L20_Mbit ) = get_factorA(20,i,p);
            temp.data( (2*p+1)*L20_Mbit + L20_Mbit-1, (2*p+1)*L20_Mbit ) = get_factorB(20,i,p);
        }
        weightsfactors_stream.write(temp);
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L21_WEIGHT_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L21_OutP; p++) {
            temp.data(L21_InP*L21_Wbit-1, 0) = get_weights(21,i,p);
            weightsfactors_stream.write(temp);
        }
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L21_FACTOR_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L21_OutP; p++) {
            temp.data( 2*p*L21_Mbit + L21_Mbit-1, 2*p*L21_Mbit ) = get_factorA(21,i,p);
            temp.data( (2*p+1)*L21_Mbit + L21_Mbit-1, (2*p+1)*L21_Mbit ) = get_factorB(21,i,p);
        }
        weightsfactors_stream.write(temp);
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L22_WEIGHT_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L22_OutP; p++) {
            temp.data(L22_InP*L22_Wbit-1, 0) = get_weights(22,i,p);
            weightsfactors_stream.write(temp);
        }
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L22_FACTOR_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L22_OutP; p++) {
            temp.data( 2*p*L22_Mbit + L22_Mbit-1, 2*p*L22_Mbit ) = get_factorA(22,i,p);
            temp.data( (2*p+1)*L22_Mbit + L22_Mbit-1, (2*p+1)*L22_Mbit ) = get_factorB(22,i,p);
        }
        weightsfactors_stream.write(temp);
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L23_WEIGHT_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L23_OutP; p++) {
            temp.data(L23_InP*L23_Wbit-1, 0) = get_weights(23,i,p);
            weightsfactors_stream.write(temp);
        }
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L23_FACTOR_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L23_OutP; p++) {
            temp.data( 2*p*L23_Mbit + L23_Mbit-1, 2*p*L23_Mbit ) = get_factorA(23,i,p);
            temp.data( (2*p+1)*L23_Mbit + L23_Mbit-1, (2*p+1)*L23_Mbit ) = get_factorB(23,i,p);
        }
        weightsfactors_stream.write(temp);
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L30_WEIGHT_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L30_OutP; p++) {
            temp.data(L30_InP*L30_Wbit-1, 0) = get_weights(30,i,p);
            weightsfactors_stream.write(temp);
        }
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L30_FACTOR_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L30_OutP; p++) {
            temp.data( 2*p*L30_Mbit + L30_Mbit-1, 2*p*L30_Mbit ) = get_factorA(30,i,p);
            temp.data( (2*p+1)*L30_Mbit + L30_Mbit-1, (2*p+1)*L30_Mbit ) = get_factorB(30,i,p);
        }
        weightsfactors_stream.write(temp);
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L31_WEIGHT_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L31_OutP; p++) {
            temp.data(L31_InP*L31_Wbit-1, 0) = get_weights(31,i,p);
            weightsfactors_stream.write(temp);
        }
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L31_FACTOR_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L31_OutP; p++) {
            temp.data( 2*p*L31_Mbit + L31_Mbit-1, 2*p*L31_Mbit ) = get_factorA(31,i,p);
            temp.data( (2*p+1)*L31_Mbit + L31_Mbit-1, (2*p+1)*L31_Mbit ) = get_factorB(31,i,p);
        }
        weightsfactors_stream.write(temp);
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L32_WEIGHT_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L32_OutP; p++) {
            temp.data(L32_InP*L32_Wbit-1, 0) = get_weights(32,i,p);
            weightsfactors_stream.write(temp);
        }
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L32_FACTOR_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L32_OutP; p++) {
            temp.data( 2*p*L32_Mbit + L32_Mbit-1, 2*p*L32_Mbit ) = get_factorA(32,i,p);
            temp.data( (2*p+1)*L32_Mbit + L32_Mbit-1, (2*p+1)*L32_Mbit ) = get_factorB(32,i,p);
        }
        weightsfactors_stream.write(temp);
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L33_WEIGHT_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L33_OutP; p++) {
            temp.data(L33_InP*L33_Wbit-1, 0) = get_weights(33,i,p);
            weightsfactors_stream.write(temp);
        }
        ofs << hex << temp.data << dec << endl;
    }
    for (unsigned i = 0; i < L33_FACTOR_ITERATIONS; i++) {
        ap_axis temp;
        temp.data = 0;
        for (unsigned p = 0; p < L33_OutP; p++) {
            temp.data( 2*p*L33_Mbit + L33_Mbit-1, 2*p*L33_Mbit ) = get_factorA(33,i,p);
            temp.data( (2*p+1)*L33_Mbit + L33_Mbit-1, (2*p+1)*L33_Mbit ) = get_factorB(33,i,p);
        }
        weightsfactors_stream.write(temp);
        ofs << hex << temp.data << dec << endl;
    }
    ofs.close();
    cout << "Writing weights complete" << endl;

    stream<ap_axis > empty;

    stream<ap_axis > outputStream1("outputStream1");
    halfsqueezenet(weightsfactors_stream, empty,0);
    halfsqueezenet(inputStream, outputStream1,1);

    unsigned temp_size = outputStream1.size();
    ofstream outfs ("output.txt", ofstream::out);
    for (unsigned j = 0; j < temp_size; j++) {
        ap_axis temp = outputStream1.read();
        outfs << temp.data << endl;
    }

    return 0;
}

#endif
